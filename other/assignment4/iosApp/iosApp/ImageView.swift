//
//  ImageView.swift
//  iosApp
//
//  Created by mobiledev on 4/16/15.
//  Copyright (c) 2015 vinitskys. All rights reserved.
//

import UIKit

class ImageView: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addTarget(target: AnyObject?, action: Selector, forControlEvents controlEvents: UIControlEvents){
        
    }
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var moose: UIImageView!
    @IBAction func sliderValueChanged(sender: UISlider) {
        
        var currentValue = CGFloat(sender.value)
        
        var mooseImage = moose.image?.CGImage
        
        var newScale : CGFloat
        
        newScale = 10 - currentValue
        
        var newImage = UIImage(CGImage: mooseImage, scale : newScale, orientation : UIImageOrientation.Up)
        
        moose.image = newImage
        
        
    }
}

